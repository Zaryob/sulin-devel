#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2008, TUBITAK/UEKAE
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# Please read the COPYING file.
#

# This python script generates the packages db needed by command-not-found.
# Must be run on buildfarm.

import glob
import os
import pickle
import sys
import inary

# Append a local copy of trunk/inary for using Pisi API without having
# root privileges on buildfarm.
# sys.path.append("/home/ozan/inary/")

if __name__ == "__main__":

    directory = "/var/cache/inary/package/"
    try:
        directory = sys.argv[1]
    except KeyError:
        pass

    d = {}
    file_list = [f for f in glob.glob("%s/*.inary" % directory) if not f.endswith(".delta.inary")]

    for p in file_list:
        print("Processing %s.." % p)
        for f in [x for x in inary.package.Package(p).get_files().list if x.type=="executable"]:
            fpath = os.path.join("/", f.path)
            if os.access(fpath, os.X_OK):
                d[fpath] = inary.util.split_package_filename(os.path.basename(p))[0]

    o = open("packages.db", "wb")
    pickle.Pickler(o, protocol=2)
    pickle.dump(d, o, protocol=2)
    o.close()

