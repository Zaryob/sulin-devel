#!/usr/bin/python
#-*- coding: utf-8 -*-

import os
import os.path
import sys
import glob
import shutil
from distutils.core import setup, Extension
from distutils.command.install import install

import sulin

distfiles = """
    setup.py
    sulin/*.py
    sulin/*.c
    sulin/xorg/*.py
    sulin/xorg/*.c
    po/*.po
    po/*.pot
    tools/*.py
    tools/*.sh
    MODULES
    README
"""

def make_dist():
    distdir = "sulin-python-%s" % sulin.versionString()
    list = []
    for t in distfiles.split():
        list.extend(glob.glob(t))
    if os.path.exists(distdir):
        shutil.rmtree(distdir)
    os.mkdir(distdir)
    for file_ in list:
        cum = distdir[:]
        for d in os.path.dirname(file_).split('/'):
            dn = os.path.join(cum, d)
            cum = dn[:]
            if not os.path.exists(dn):
                os.mkdir(dn)
        shutil.copy(file_, os.path.join(distdir, file_))
    os.popen("tar -czf %s %s" % ("sulin-python-" + sulin.versionString() + ".tar.gz", distdir))
    shutil.rmtree(distdir)

if "dist" in sys.argv:
    make_dist()
    sys.exit(0)

class Install(install):
    def finalize_options(self):
        # NOTE: for Sulin distribution
        if os.path.exists("/etc/sulin-release"):
            self.install_platlib = '$base/lib/sulin'
            self.install_purelib = '$base/lib/sulin'
        install.finalize_options(self)

    def run(self):
        install.run(self)
        self.installi18n()

    def installi18n(self):
        for name in os.listdir('po'):
            if not name.endswith('.po'):
                continue
            lang = name[:-3]
            print("Installing '%s' translations..." % lang)
            os.system("msgfmt po/%s.po -o po/%s.mo" % (lang, lang))
            if not self.root:
                self.root = "/"
            destpath = os.path.join(self.root, "usr/share/locale/%s/LC_MESSAGES" % lang)
            if not os.path.exists(destpath):
                os.makedirs(destpath)
            shutil.copy("po/%s.mo" % lang, os.path.join(destpath, "sulin-python.mo"))

setup(name="sulin",
      version=sulin.versionString(),
      description="Python Modules for Sulin",
      long_description="Python Modules for Sulin.",
      license="GNU GPL2",
      author="Sulin Developers",
      author_email="info@sulin.org.tr",
      url="http://www.sulin.org.tr/",
      packages = ['sulin', 'sulin.xorg'],
      ext_modules = [Extension('sulin.xorg.capslock',
                               sources=['sulin/xorg/capslock.c'],
                               libraries=['X11']),
                     Extension('sulin.csapi',
                               sources=['sulin/csapi.c'],
                               libraries=[]),
                    ],
      cmdclass = {'install' : Install})
